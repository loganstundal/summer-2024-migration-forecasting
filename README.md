# summer-2024-migration-forecasting

## Directory structure

```
.
├── 10-data
│   ├── 01-original
│   ├── 02-tidy
│   └── 03-model
├── 20-scripts
│   ├── 01-tidy
│   ├── 02-model
│   └── 03-descriptives
├── 30-results
│   ├── 01-figures
│   ├── 02-models
│   └── 03-tables
├── 40-memos
└── 50-docs
    ├── 01-talks
    └── 02-paper
```


## to clone

```
cd existing_repo
git remote add origin https://gitlab.com/loganstundal/summer-2024-migration-forecasting.git
git branch -M main
git push -uf origin main
```